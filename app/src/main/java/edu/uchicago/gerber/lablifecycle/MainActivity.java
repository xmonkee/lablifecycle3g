package edu.uchicago.gerber.lablifecycle;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {


    public static final String TAG = "LCYCLE";
    private int mConfigChangeNum;
    private EditText mEditTextName;

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(new Object(){}.getClass().getEnclosingMethod().getName().toString(), TAG);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(new Object(){}.getClass().getEnclosingMethod().getName().toString(), TAG);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mEditTextName = (EditText) findViewById(R.id.editText);

        Log.d(new Object(){}.getClass().getEnclosingMethod().getName().toString(), TAG);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {

        //int nNum =  savedInstanceState.getInt("config_change");
        mConfigChangeNum = savedInstanceState.getInt("config_change");
        Toast.makeText(this, "configured "+ String.valueOf(mConfigChangeNum) +" times", Toast.LENGTH_SHORT).show();


        super.onRestoreInstanceState(savedInstanceState);
        Log.d(isNull(savedInstanceState) + ":"+  new Object(){}.getClass().getEnclosingMethod().getName().toString(), TAG);
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.d(new Object(){}.getClass().getEnclosingMethod().getName().toString(), TAG);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {


        outState.putInt("config_change",++mConfigChangeNum );
        super.onSaveInstanceState(outState);
        Log.d( isNull(outState) + ":"+    new Object(){}.getClass().getEnclosingMethod().getName().toString(), TAG);
    }




    @Override
    protected void onPause() {
        super.onPause();
        Log.d(new Object(){}.getClass().getEnclosingMethod().getName().toString(), TAG);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(new Object(){}.getClass().getEnclosingMethod().getName().toString(), TAG);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(new Object(){}.getClass().getEnclosingMethod().getName().toString(), TAG);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


   private String isNull(Bundle bundle){
      return bundle == null ? "null" : "full";
   }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
